<aside class="page-contact" id="page-contact">
  <div class="page-contact__section">
    <?= $site->cta()->kirbytext() ?>
  </div>
  <div class="page-contact__section" id="page-contact-form">

    <?php if($success): ?>
      <p><strong><em>Vielen Dank für Ihr Interesse.</em></strong></p>
      <p>Eine E-Mail mit Ihrer Anfrage wurde erfolgreich versendet. Wir melden uns umgehend bei Ihnen.</p>
    <?php else: ?>

      <?php if (isset($alert['error'])): ?>
        <p><?= $alert['error'] ?></p>
      <?php endif ?>

      <form class="form" method="post" action="<?= $page->url() ?>#page-contact-form">
        <div class="form__field">
          <label class="form__field__label" for="name">Name*</label>
          <input class="form__field__text" type="text" id="name" name="name" value="<?= $data['name'] ?? '' ?>" required>
          <?= isset($alert['name']) ? '<span class="form__field__error">' . html($alert['name']) . '</span>' : '' ?>
        </div>
        <div class="form__field">
          <label class="form__field__label" for="email">E-Mail*</label>
          <input class="form__field__text" type="email" id="email" name="email" required>
          <?= isset($alert['email']) ? '<span class="form__field__error">' . html($alert['email']) . '</span>' : '' ?>
        </div>
        <div class="form__field">
          <label class="form__field__label" for="url">Webseite</label>
          <input class="form__field__text" type="text" id="url" name="url">
          <?= isset($alert['url']) ? '<span class="form__field__error">' . html($alert['url']) . '</span>' : '' ?>
        </div>
        <div class="form__field">
          <label class="form__field__label" for="phone">Telefon</label>
          <input class="form__field__text" type="text" id="phone" name="phone">
          <?= isset($alert['phone']) ? '<span class="form__field__error">' . html($alert['phone']) . '</span>' : '' ?>
        </div>
        <div class="form__field form__field--honeypot">
          <label class="form__field__label" for="website">Webseite</label>
          <input class="form__field__text" type="text" id="website" name="website">
        </div>
        <div class="form__field">
          <input class="form__field__btn" type="submit" name="submit" value="Absenden">
        </div>
      </form>

    <?php endif ?>
  </div>
</aside>
