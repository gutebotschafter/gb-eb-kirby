<header class="page-header">
  <?php if($site->coverImage()->isNotEmpty() and $page->isHomePage()): ?>
    <h1 class="page-header__image">
      <img src="<?= $site->coverImage()->toFile()->url() ?>" alt="<?= $site->title()->html() ?> – <?= $site->claim()->html() ?>">
    </h1>
  <?php else: ?>
    <h1 class="page-header__title">
      <?php if($page->isHomePage()): ?>
        <?= $site->title()->html() ?>
      <?php else: ?>
        <?= $page->title()->html() ?>
      <?php endif ?>
    </h1>
  <?php endif ?>
  <?php if($site->coverBackground()->toFile()): ?>
    <div class="page-header__background<?= e($page->isHomePage(), ' page-header__background--home') ?>" style="background-image: url('<?= $site->coverBackground()->toFile()->url() ?>');"></div>
  <?php endif ?>
</header>
