<section class="ce-highlights" id="<?= $data->slug() ?>">
  <div class="ce-highlights__title">
    <h2><?= $data->headline()->kti() ?></h2>
    <?php if($data->sub()->isNotEmpty()): ?>
      <p><?= $data->sub()->kti() ?></p>
    <?php endif ?>
  </div>
  <div class="ce-highlights__entries">
    <?php foreach($data->highlights()->toStructure() as $item): ?>
      <figure class="ce-highlights__item">
        <img src="<?= $item->picture()->toFile()->thumb(['width' => 480, 'height' => 320, 'crop' => true, 'quality' => 90])->url() ?>" alt="<?= $item->picture()->toFile()->alt()->html() ?>">
        <figcaption>
          <h3><?= $item->title()->widont() ?></h3>
          <?= $item->text()->kt() ?>
          <?php if($item->btnLink()->isNotEmpty()): ?>
            <a class="btn" href="<?= $item->btnLink() ?>" rel="noopener" title="<?= e($item->btnText()->isNotEmpty(), $item->btnText()->html(), 'Mehr erfahren') ?>">
              <?= e($item->btnText()->isNotEmpty(), $item->btnText()->html(), 'Mehr erfahren') ?>
            </a>
          <?php endif ?>
        </figcaption>
      </figure>
    <?php endforeach ?>
  </div>
</section>
