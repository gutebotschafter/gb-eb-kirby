#!/usr/bin/env bash

DOCKER_PHP=$(docker ps | grep _php_ | cut -d' ' -f1)
DOCKER_NGINX=$(docker ps | grep _web_ | cut -d' ' -f1)

echo "Installing npm dependencies..."
docker exec -it ${DOCKER_NGINX} sh -c "cd /var/www/html && npm install"
echo ""
echo "Installing composer dependencies"
docker exec -it ${DOCKER_PHP} sh -c "cd /var/www/html && php composer.phar self-update && php composer.phar install"
echo ""
echo "All done! You can now execute ./webpack-dev-server.sh"
echo ""
